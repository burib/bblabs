var express = require("express"),
	app = express(),
    exphbs  = require('express3-handlebars'),
	io,
	port = 1337;


app.configure(function () {
	app.set('views', __dirname + '/views');
	/**
	 * handlebars template engine
	 */
	
	app.set('view engine', 'handlebars');
	app.engine('handlebars', exphbs({defaultLayout: ''}));
	
});
app.get("/", function(req, res){
    res.render("page");
});

app.use(express.static(__dirname + '/public'));
 
io = require('socket.io').listen(app.listen(port));


io.sockets.on('connection', function (socket) {
    socket.emit('message', { message: 'welcome to the chat' });
    socket.on('send', function (data) {
        io.sockets.emit('message', data);
    });
});


console.log("Listening on port " + port);