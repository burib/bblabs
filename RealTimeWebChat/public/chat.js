window.onload = function() {
    isActive = true;

    var messages = [],
        socket = io.connect('http://10.0.7.31:1337'),
        message = document.getElementById("message"),
        chatForm = document.getElementById("chatForm"),
        content = document.getElementById("chatContent"),
        username = document.getElementById("username"),
        audioContext,
        newMsgSound = null;

    function setupAudio () {
      try {
        // Fix up for prefixing
        window.AudioContext = window.AudioContext||window.webkitAudioContext;
        audioContext = new AudioContext();
        return true;
      }
      catch(e) {
        console.warn('Web Audio API is not supported in this browser');
        return false;
      }
    };

    function loadSound(url) {
      var request = new XMLHttpRequest();
      request.open('GET', url, true);
      request.responseType = 'arraybuffer';

      // Decode asynchronously
      request.onload = function() {
        audioContext.decodeAudioData(request.response, function(buffer) {
          newMsgSound = buffer;
        }, function (err) {
            console.log(err);
        });
      }
      request.send();
    };

    function playSound(buffer) {
        var source = audioContext.createBufferSource(); // creates a sound source
        source.buffer = buffer;                    // tell the source which sound to play
        source.connect(audioContext.destination);       // connect the source to the context's destination (the speakers)
        source.start(0);                           // play the source now
    };

    setupAudio() && loadSound('sounds/newmsg.mp3');

    function hasGetUserMedia() {
      // Note: Opera is unprefixed.
      return !!(navigator.getUserMedia || navigator.webkitGetUserMedia ||
                navigator.mozGetUserMedia || navigator.msGetUserMedia);
    }

    if (hasGetUserMedia()) {
      console.log('media is supported');
    } else {
      console.warn('getUserMedia() is not supported in your browser');
    }

    socket.on('message', function (data) {
        if(data.message) {
            messages.push(data);
            var html = '',
                username;
            for(var i=0; i<messages.length; i++) {
                username = messages[i].username ? messages[i].username : 'Server';
                html += '<span class="username '+username+'">' + username + ': </span>';
                html += messages[i].message + '<br />';
                if (username !== 'Server' && !window.isActive) {
                    playSound(newMsgSound);
                }
            }
            content.innerHTML = html;
        } else {
            console.log("There is a problem:", data);
        }
    });

    (function() {
        var hidden = "hidden";

        // Standards:
        if (hidden in document) {
            document.addEventListener("visibilitychange", onchange);
        }
        else if ((hidden = "mozHidden") in document) {
            document.addEventListener("mozvisibilitychange", onchange);
        }
        else if ((hidden = "webkitHidden") in document) {
            document.addEventListener("webkitvisibilitychange", onchange);
        }
        else if ((hidden = "msHidden") in document) {
            document.addEventListener("msvisibilitychange", onchange);
        }
        // IE 9 and lower:
        else if ('onfocusin' in document) {
            document.onfocusin = document.onfocusout = onchange;
        }
        // All others:
        else {
            window.onpageshow = window.onpagehide
                = window.onfocus = window.onblur = onchange;
        }

        function onchange (evt) {
            var v = 'visible', h = 'hidden',
                evtMap = {
                    focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h
                };

            evt = evt || window.event;
            if (evt.type in evtMap)
                window.isActive = evtMap[evt.type];
            else
                window.isActive = this[hidden] ? false : true;
        }
    })();

     chatForm.onsubmit = function(e) {
        e.preventDefault();
        if(username.value == "") {
            alert("Please type your username!");
        } else {
            var text = message.value;
            socket.emit('send', { message: text, username: username.value });
            message.value = "";

        }
    };

}