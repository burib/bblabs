(function($, window, undefined){
    // declare the module
    var App = function () {
        // set some configuation
        var config = {
            nojsClass: "nojs"
        };

        var init = function () {
            nojs();
        };

        var nojs = function () {
            $('body').removeClass(config.nojsClass);
        };

        return {
            init: init
        }
    }();

    $(function(){
        App.init();
    });



})(jQuery, window);