(function() {
    // Require.js allows us to configure shortcut alias
    require.config({
        baseUrl: '/js',
        paths: {
            jquery: '/js/lib/jquery/jquery-1.9.1',
        }
    });

    require([
      // Load our app module and pass it to our definition function
      'app',
    ], function(App){
      // The "app" dependency is passed in as "App"
      App.initialize();
    });
})();


