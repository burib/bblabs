var express = require('express'),
    app = express();

app.use(express.static(__dirname));

app.get('/', function (req, res) {
    res.redirect('index.html');
});

app.listen(1526);
console.log('Express server started on port %s', 1526);

